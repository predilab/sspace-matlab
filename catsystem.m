function sys= catsystem(varargin)
% CATSYSTEM   Concatenates any number of State Space systems in one single
%
%  sys= catsystem(sys_1, sys_2, ...);
%
% INPUTS:
%  sys_1:   System matrices of first SS model
%  sys_2:   System matrices of second SS model
%  ...  :   System matrices of the rest of SS models
%  
% OUTPUTS:
%  sys  :    System matrices of concatenated system
sys= catsystem0(varargin{:});

