function [bands, std2]= confband(zhat, Pz, const)
% confband  Confidence bands of states, forecasts, predictions, etc.
%
%   bands= confband(fit, F, constant)
%
%           The inputs to this function are usually the output of functions SSfilter, 
%           SSsmooth or SSdisturb, stored on the fields of a SSpace system created 
%           previously with :keyword:`SSmodel`.
%
% INPUTS:
%  fit:      Filtered or smoothed states, fitted values, forecasts, etc. (.a, .yfit, 
%            .yfor, etc., see SSmodel)
%  F:        Covariance matrix of fit (.P, .F, .Ffor, etc., see SSmodel)
%  constant: Constant for confidence band (2 for 95% confidence)
%
% OUTPUTS:
%  bands:  Upper and lower confidence bands, i.e. fit+-constant*sqrt(P)

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

if nargin< 3
    [bands, std2]= confband0(zhat, Pz);
else
    [bands, std2]= confband0(zhat, Pz, const);
end