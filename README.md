#                 SSpace Toolbox for State Space systems
#                             Version 0.1.0 
  
##  What is it?
  SSpace is a MATLAB toolbox providing a number of routines designed for a general analysis of State Space systems combining both flexibility and simplicity, enhancing the power and versatility of SS modeling, all together in a easy-to-use framework. 

##  Downloading
  The current version can be found under https://bitbucket.org/predilab/sspace-matlab/downloads/SSpace.mltbx

##  Installation
  Just drop the mltbx file (MATLAB Toolbox) in the MATLAB prompt and follow the wizard.

##  Documentation
  The documentation available as of the date of this release is provided in PDF format in the same toolbox directory. Also available here: https://bitbucket.org/predilab/sspace-matlab/downloads/SSpace_documentation.pdf

##  Contents
  The SSpace Toolbox is structured as follows:
###  Core functions.
    SSmodel       - Creates SSpace model object or adds properties to an existing one
    SSestim       - Estimation of a SSpace model
    SSvalidate    - Validation of a SSpace model
    SSfilter      - Optimal kalman filtering of SSpace model
    SSsmooth      - Optimal fixed interval smoothing of SSpace model
    SSdisturb     - Optimal disturbance smoother
 
###  Model templates.
	  SampleSS       - General SS system
	  SampleARIMA    - ARIMA models with exogenous inputs
	  SampleBSM      - Basic Structural Model
	  SampleDHR      - Dynamic Harmonic Regression
	  SampleDLR      - Dynamic Linear Regression
	  SampleES       - Exponential smoothing
	  SampleEXP      - Non gaussian models (exponential family of models)
	  SampleNONGAUSS - Non gaussian models (models with non-gaussian observation noise)
	  SampleSV       - Non gaussian models (Stochastich Volatility)
	  SampleNL       - Non Linear model (estimated with Extended Kalman Filter)
	  SampleAGG      - Models with time aggregation
	  SampleCAT      - Concatenation of State Space systems
	  SampleNEST     - Nesting in inputs State Space systems
 
###  Additional helpful functions.
    confband        - Confidence intervals of forecasts
    constrain       - Free constraints of parameters
    varmatrix       - Semidefinite positive covariance matrices
    optimizer       - Optimizer options
    normalize       - Variable normalization (standarization)
    vdiff           - Differentiation of vector time series
 
###  Demonstrations.
    SSdemo          - SSpace demos

###  Documentation.
    SSpace_documentation - User manual in PDF format

##  Bug Reporting
  You can send SSpace bug reports to <https://bitbucket.org/predilab/sspace-matlab/issues>.  
  Please see the section of the GNU make manual entitled 'Problems and Bugs' for  information on submitting useful and complete bug reports.

  If you need help using SSpace, please first search for a related question in these forums, or write your own one if there is nothing about.

##  Licensing
  Please see the file called LICENSE.
  
##  Contact
E-mail: 

diego.pedregal at uclm.es

marco.villegas at uclm.es

http://www.uclm.es/profesorado/diego/

http://solid-analytics.com/sspace-doc

Copyright � 2017 by Diego J. Pedregal & Marco A. Villegas. 

All rights reserved.