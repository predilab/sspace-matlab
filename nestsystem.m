function sys= nestsystem(sysi, syso) %, p)
% NESTSYSTEM   Nests input and output general SS systems
%
%  sys= nestsystem(sys_inputs, sys_outputs);
%
% INPUTS:
%  sys_inputs :   System matrices of inputs SS model
%  sys_outputs:   System matrices of ourputs SS model
%  
% OUTPUTS:
%  sys        :   System matrices of nested model

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

sys= nestsystem0(sysi, syso);
