function [AB, indx]= nanprod(A, B)
% nanprod  Product of two matrices with nan values
%
%   [AB, indx]= nanprod(A, B)
%
% INPUTS:
%  A:      First matrix
%  B:      Second matrix
%
% OUTPUTS:
%  AB:     Product of the two matrices avoiding nan values
%  indx:   Number of non-NaN values at eac position in AB product matrix
[AB, indx]= nanprod0(A, B);
