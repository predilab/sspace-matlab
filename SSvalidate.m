function sys= SSvalidate(sys, numeric)
% SSvalidate  Validation of estimated SS system. Prints out a table with 
%             diagnosis information.It produces the same output than SSfilter,
%             and also the table output in .table.
%
% sys= SSvalidate(sys)
%
% INPUTS:
%  sys:    SSpace model object created with SSmodel and estimated with SSestim
%
% OUTPUTS:
%  sys:    Input system with the estimation output

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

if nargin< 2
    sys= SSvalidate0(sys);
else
    sys= SSvalidate0(sys, numeric);
end
