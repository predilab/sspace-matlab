function sys= SSmodel(varargin)
% SSmodel  Creates SSpace model object or adds properties to an existing one
%          
% sys= SSmodel('property1', value1, 'property2', value2, ...)
% sys= SSmodel(sys, 'property1', value1, 'property2', value2, ...)
%
% Properties are either set up manually by the user (inputs, marked with 
% an asterisk (*) below) or changed by functions (outputs, not configurable
% by the user). Only the leading characters, enough to identify uniquely the 
% item, are necessary.
%
% INPUTS:
%     'y':                 (*) Matrix of output data
%     'u':                 (*) Matrix of input data (if any)
%     'noutputs':          (*) Number of output series
%     'model':             (*) Handle to model function
%     'user_inputs':       (*) Cell of addional parameters for model function
%     'p0':                (*) Vector of initial parameters for search
%     'p':                 (*) Parameters vector (either estimated or fixed by the user)
%     'a1':                (*) Initial state (exact if empty or NaN's (all or part))
%     'P1':                (*) Initial diagonal of state covariance matrix 
%                              (NaNs indicate calculation of a particular initial 
%                              stateby the toolbox and Infs indicate diffuse initialisation)
%     'OBJ_FUNCTION_NAME': (*) Handle to objective function. Options at the moment are:
%                              @llik:   Log Likelihood (default)
%                              @llikc:  Concentrated Log Likelihood
%                              {@nsteps, j:N}: sum of j to N steps ahead minimal error (j > 1)
%     'gradient':          (*) Analytical gradient on/off for estimation
%     'Nsimul':            (*) Number of simulation for non-gaussian models
%     'steady_tol':        (*) Steady State tolerance (0)
%     'univariate':        (*) Univariate recursive algorithms for multivariate models (0)
%
% OUTPUTS:
%     'table':                 Table output after estimation
%     'obj_value':             Optimal value of objective function
%     'llik':                  Log likelihood value
%     'covp':                  Covariance of parameter estimates
%     'mat':                   System matrices
%     'a':                     State estimates
%     'P':                     State (co)variance estimates
%     'yfit':                  Fitted values
%     'F':                     Fitted values (co)variance
%     'yfor':                  Forecasted values
%     'Ffor':                  Forecasted (co)variance
%     'v':                     Innovations
%     'Fv':                    Innovations (co)variance
%     'eta':                   State disturbance estimates
%     'eps':                   Observation disturbance estimates
%     'Veta':                  Covariance of state disturbances
%     'Veps':                  Covariance of observation disturbances

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

sys= SSmodel0(varargin{:});
