function P= varmatrix(p, m, r)
% varmatrix  Builds a semidefinite positive covariance matrix from a vector of values
%
%    P= varmatrix(p)
%    P= varmatrix(p, m, rank)
%    P= varmatrix(p, constrains)
%
% INPUTS:
%  p:          Vector of any values (np x 1) or semidefinite positive matrix (m x m)
%  m:          Dimension of covariance matrix or zero and ones matrix
%              containing zero constraints
%  r:          Desired rank of covariance matrix when not full-rank covariance matrix is needed
%
% OUTPUTS:
%  P:  Semidefinite positive matrix of dimension m with rank rank when input p is 
%      a vector, or vector of parameters when p is a covariance matrix
%
% Examples:
%       varmatrix((1:10)'/100)
%       varmatrix((1:5)'/100, 3, 2)
%       varmatrix((1:5)'/100, [1 1 0; 1 1 1; 0 1 1])
if nargin< 2
    P= varmatrix0(p);
elseif nargin< 3
    P= varmatrix0(p, m);
elseif nargin< 4
    P= varmatrix0(p, m, r);
end


