% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleNL(p, at, ctrl)
% SampleNL  Template for non-linear State Space models
%    J. Durbin and S.J. Koopman, Time Series Analysis by State Space Methods, 
%    Second Edition. 2012, Oxford University Press
%
% DEFINITION OF NON-LINEAR STATE SPACE MODEL:
% ===============================
%    a(t+1) = Ta(a(t)) + Gam(t) + R(a(t)) eta(t)
%    y(t)   = Za(a(t)) + D(t)   + C(a(t)) eps(t)
%
%    Var(eta(t))= Q(a(t))     Var(eps(t))= H(a(t))    Cov(eta(t), eps(t))= S(a(t))
%
%    Gam(t) and D(t) may have several forms, depending on the model needed, use always
%    the simplest. In what follows u(t) are input variables. The same
%    applies to matrix D(t)
%    variables:
%        Gam(t) is (ns x 1), useful when Gam(t) is just a vector of constants for every t 
%        Gam(t)= gam u(t).    Gam(t) is (ns x t), useful when we want to make
%                             u(t) appear explicitly and gam are constant parameters
%        Gam(t)= gam(t) u(t). Gam(t) is (ns x nu x t), similar to the previous,
%                             but when gam is varying in time
%
% OUTPUTS:
%  model:    System matrices filled up with parameters p
% For linear matrices:
%     model.T: (ns x ns x (1 or n))
%     model.Gam: (empty, ns x (1 or n), ns x Nu, ns x Nu x (1 or n))
%     model.R: (ns x neps x (1 or n))
%     model.Z: (Ny x ns x (1 or n))
%     model.D: (empty, Ny x (1 or n), Ny x Nu, Ny x Nu x (1 or n))
%     model.C: (Ny x Neps x (1 or n))
%     model.Q: (neps x neps x (1 or n))
%     model.H: (Ny x Ny x (1 or n))
% For non-linear matrices (to define those matrices that dependent on at):
%     model.dTa: derivative of matrix T(at) (ns x Nsigma)
%     model.Ta: T(at) (ns x Nsigma)
%     model.Ra: R(at) (ns x neps x (1 or n))
%     model.Qa: Q(at) (neps x neps x (1 or n))
%     model.dZa: derivative of matrix Z(at) (m x Nsigma)
%     model.Za: Z(at) (m x Nsigma)
%     model.Ca: C(at) (Ny x Neps x (1 or n))
%     model.Ha: H(at) (Ny x Ny x (1 or n))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Any code could be added here %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Defining linear matrices
if ctrl< 2
    % Matrix definitions as in SampleSS
    % Code defining matrix T (ns x ns x (1 or n))
    model.T= []; 
    % Code defining matrix Gam (empty, ns x (1 or n), ns x Nu, ns x Nu x (1 or n))
    model.Gam= [];
    % Code defining matrix R (ns x neps x (1 or n))
    model.R= [];
    % Code defining matrix Z (Ny x ns x (1 or n))
    model.Z= [];
    % Code defining matrix D (empty, Ny x (1 or n), Ny x Nu, Ny x Nu x (1 or n))
    model.D= [];
    % Code defining matrix C (Ny x Neps x (1 or n))
    model.C= [];
    % Code defining matrix Q (neps x neps x (1 or n))
    model.Q= []; 
    % Code defining matrix H (Ny x Ny x (1 or n))
    model.H= [];
    % Returning parameters
    model.p= p;
    model.S= [];
end

%% Defining nonlinear matrices in State Equation
if any(ctrl== [2 0])
    % Code defining derivative of matrix T(at) (ns x Nsigma)
    % NECESSARY ONLY IN CASE EXTENDED KALMAN FILTERED IS USED!!
    model.dTa= [];
    % Code defining matrix T(at) (ns x Nsigma)
    model.Ta= [];
    % Code defining matrix R(at) (ns x neps x (1 or n))
    model.Ra= [];
    % Code defining matrix Q(at) (neps x neps x (1 or n))
    model.Qa= []; 
end

%% Defining nonlinear matrices in Observation Equation
if any(ctrl== [3 0])
    % Code defining derivative of matrix Z(at) (m x Nsigma)
    % NECESSARY ONLY IN CASE EXTENDED KALMAN FILTERED IS USED!!
    model.dZa= [];
    % Code defining matrix Z(at) (m x Nsigma)
    model.Za= [];
    % Code defining matrix C(at) (Ny x Neps x (1 or n))
    model.Ca= [];
    % Code defining matrix H(at) (Ny x Ny x (1 or n))
    model.Ha= [];
end
model.p= p;
