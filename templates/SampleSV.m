% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleSV(p, H)
% SampleSV  Template for Stochastic Volatility models
%    J. Durbin and S.J. Koopman, Time Series Analysis by State Space Methods, 
%    Second Edition. 2012, Oxford University Press
%
% y(t) = Mu + Sigma * exp(0.5*THETA(t)) u(t)
% THETA(t+1) = Phi THETA(t) + eta(t)          (0< Phi < 1)
%   Beware that the 'H' input to this function, i.e. the observationa noise
%   variance, is compulsory and should be passed on to the SSsystem in line 13.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mean Mu
Mu= 0;
% Sigma2 (variance!) value
Sigma2= varmatrix();
% AR(1) parameter (Phi)
AR1= constrain(p(some_item), [0 1]);
% AR(1) variance noise
Q= varmatrix();
% Exogenous effects
D= [];

% Translating to SS form
model= SV2SS(Sigma2, AR1, Q, D, H);
% Returning parameters
model.p= p;

