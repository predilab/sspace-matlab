% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleDHR(p, N)
% SampleDHR  Template for Multivariate Dynamic Harmonic Regression
%     Young, P.C., Pedregal, D.J., Tych, W. (1999), Dynamic Harmonic 
%     Regression, Journal of Forecasting, 18, 369-394.
%
% y(t) = T(t) + C(t) + S(t) + f(u(t)) + I(t)
%
% y(t):    set of m output variables
% u(t):    set of k input variables
% T(t):    trend components
% C(t):    cyclical components (either trigonommetric or dummy)
% S(t):    seasonal components (either trigonommetric or dummy)
% f(u(t)): relation of inputs to outputs. It may be linear, non linear
%          regression, time varying regression, multiple transfer function, etc.
%          some of the options should be implemented with the help of the
%          catsystem function
% I(t):    irregular components

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Trend model
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= is_the_number_of_outputs;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = blkdiag(varmatrix(), varmatrix());

% Seasonal/cyclical DHR components
% Period are an m x Nhar matrix, where Nhar is the number of harmonics. NaN
%    indicate that such periodic component is not present in a time series
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs = [Q(1) Q(2) ... Q(Nhar)], where each Qi is a squared covariance matrix
Periods = some_periods;
Rho = some_rho;
Qs = repmat(varmatrix(), [], []);

% Linear Regression effects (add u(t) inputs to the inputs of this function)
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= varmatrix();

% IRREGULAR COMPONENT (often empty if VAR component is included)
H = varmatrix();

% Translating to SS form
model= vdhr2ss(TT, ZT, RT, QT, Periods, Rho, Qs, N, H, D, VAR, HVAR, p);

