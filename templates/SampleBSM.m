function model= SampleBSM(p)
% SampleBSM  Template for Multivariate Basic Structural Model with dummy or 
%            trigonommetric seasonality
%     A.C. Harvey(1989), "Forecasting, Structural Time Series Models 
%     and the Kalman Filter," Cambridge Books, Cambridge University Press
%
% y(t) = T(t) + C(t) + S(t) + f(u(t)) + VAR(t) + I(t)
%
% y(t):    set of m output variables
% u(t):    set of k input variables
% T(t):    trend components
% C(t):    cyclical components (either trigonommetric or dummy)
% S(t):    seasonal components (either trigonommetric or dummy)
% f(u(t)): relation of inputs to outputs. It may be linear, non linear
%          regression, time varying regression, multiple transfer function, etc.
%          some of the options should be implemented with the help of the
%          catsystem function
% VAR(t):  Vector autorregressive component (small order)
% I(t):    irregular components

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TREND MODEL
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= number_of_outputs;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = blkdiag(varmatrix(), varmatrix());

% TRIGONOMMETRIC SEASONAL MODEL
% Period are an m x Nhar matrix, where Nhar is the number of harmonics. NaN
%    indicate that such periodic component is not present in a time series
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs= [Q(1) Q(2) ... Q(Nhar)], where each Q(i) is a squared covariance matrix
Periods= [];
Rho= [];
Qs= repmat(varmatrix(), 1, additional_parameters);

% DUMMY VARIABLE SEASONAL MODEL
% Period is the period of annual seasonality (just a scalar).
% Qs is the covariance matrix of eta disturbances for the seasonal component
% Rho= ;
% Period = ;
% Qs = varmatrix();

% LINEAR REGRESSION TERMS
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= varmatrix();

% IRREGULAR COMPONENT (often empty if VAR component is included)
H = varmatrix();

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);

