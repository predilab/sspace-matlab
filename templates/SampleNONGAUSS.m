% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleNONGAUSS(p, H)
% SampleNONGAUSS Template for models with non-gaussian observed noise
%    J. Durbin and S.J. Koopman, Time Series Analysis by State Space Methods, 
%    Second Edition. 2012, Oxford University Press
%
% y(t) = THETA(t) + EPS(t),
%   Beware that the 'H' input to this function, i.e. the observationa noise
%   variance, is compulsory and should be passed on to the SSsystem in line 15.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General SS system of your choice.
model= SSsystem(p, H, additional_parameters);

% Choose any member of the exponential family in model.dist from the list
%       below for the observation equation and use all the additional
%       parameters (model.param) necessary to work out the distribution:
%   Binary
%   Binomial(n)
%   Exponential
%   NegativeBinomial
%   Poisson
%   Tdist (T distribution)
% Degrees of freedom
dof= constrain(p(some_item), [2 200]);
model.dist= Tdist;
% Variance of EPSt
Sigma2= varmatrix();
model.param= {dof, Sigma2};

% Returning parameters
model.p= p;
model.noise= [];

