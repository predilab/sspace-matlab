% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleSS(p)
% SampleSS  Template for general State Space model
%
% DEFINITION OF STATE SPACE MODEL:
% ===============================
%    a(t+1) = T(t) a(t) + Gam(t) + R(t) eta(t)
%    y(t)   = Z(t) a(t) + D(t)   + C(t) eps(t)
%
%    Var(eta(t))= Q(t)     Var(eps(t))= H(t)    Cov(eta(t), eps(t))= S(t)
%
%    Gam(t) and D(t) admits several forms, depending on the model needed, use always
%    the simplest. In what follows u(t) are input variables. The same
%    applies to matrix D(t):
%        Gam(t) is (ns x 1),    useful when Gam(t) is just a vector of constants for every t 
%        Gam(t)= gam * u(t).    Gam(t) is (ns x t), useful when we want to make
%                               u(t) appear explicitly and gam are constant parameters
%        Gam(t)= gam(t) * u(t). Gam(t) is (ns x nu x t), similar to the previous,
%                               but when gam is varying in time

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code defining matrix T (ns x ns x (1 or n))
model.T= []; 

% Code defining matrix Gam (empty, ns x (1 or n), ns x Nu, ns x Nu x (1 or n))
model.Gam= [];

% Code defining matrix R (ns x Neps x (1 or n))
model.R= [];

% Code defining matrix Z (Ny x ns x (1 or n))
model.Z= [];

% Code defining matrix D (empty, Ny x (1 or n), Ny x Nu, Ny x Nu x (1 or n))
model.D= [];

% Code defining matrix C (Ny x Neps x (1 or n))
model.C= [];

% Code defining matrix Q (Neps x Neps x (1 or n))
model.Q= []; 

% Code defining matrix H (Ny x Ny x (1 or n))
model.H= [];

% Code defining matrix S (empty, Neta x Neps x (1 or n))
model.S= [];

% Returning parameters
model.p= p;


