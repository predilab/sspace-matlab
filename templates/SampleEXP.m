% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleEXP(p, H)
% SampleEXP Template for models with observation equation from the 
%           exponential family of models
%    J. Durbin and S.J. Koopman, Time Series Analysis by State Space Methods, 
%    Second Edition. 2012, Oxford University Press
%
% p (y(t)|THETA(t)) = exp[y(t)'THETA(t) b(t)(THETA(t)) + c(t)(y(t))]
%   Beware that the 'H' input to this function, i.e. the observationa noise
%   variance, is compulsory and should be passed on to the SSsystem in line 15.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General SS system of your choice.
model= SSsystem(p, H, additional_parameters);

% Choose any member of the exponential family from these cases
%       for the observation equation:
%   Binary
%   Binomial(n)
%   Exponential
%   NegativeBinomial
%   Poisson
model.dist= Poisson;

% Returning parameters
model.p= p;
model.param= {};
