% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleVARMAX(p, stationary)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For a set o m output variables and Nu input variables
% [     I   + ARpoly(1)*B + ...] * Diffy(B) * y(t) = 
% [Omega(0) + Omega(1)*B  + ...]            * u(t) + [I + MApoly(1)*B + ...] * a(t)
% Diffy is the difference polynomial necessary to produce stationarity in the outpus, e.g.
%    [I; -I] for one difference of all outputs
%    [I; -2*I; I] for two differences of all outputs, etc.
% ARpoly = [ARpoly(0); ARpoly(1); ...]; is the matrix of AR mxm block coefficients
% MApoly = [MApoly(0); MApoly(1); ...]; is the matrix of MA mxm block coefficients
% Omega=   [ Omega(0);  Omega(1); ...]; is the matrix of Omega mxNu block coefficients
% Sigma is the covariance matrix of m noises (a(t))
m= [];
I= eye(m);
Sigma = [];
Diffy= [];
ARpoly = [];
MApoly = [];
Omega= [];

% Translating to SS form
if stationary
    DD= I;
else
    DD= Diffy;
end
model= varmax2ss(DD, ARpoly, MApoly, Sigma, Omega, m, p, []);

