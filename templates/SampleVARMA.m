% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleVARMA(p, stationary)
% SampleNONGAUSS Template for models with non-gaussian observed noise

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Arbitrary function input-output model
% For a set o m output variables and Nu input variables
% Diffy(B) * y(t) = 
%                   D + inv([     I   + ARpoly(1)*B + ...])[I + MApoly(1)*B + ...] * a(t)
% Diffy is the difference polynomial necessary to produce stationarity in the outpus, e.g.
%    [I; -I] for one difference of all outputs
%    [I; -2*I; I] for two differences of all outputs, etc.
% ARpoly = [ARpoly(0); ARpoly(1); ...]; is the matrix of AR mxm block coefficients
% MApoly = [MApoly(0); MApoly(1); ...]; is the matrix of MA mxm block coefficients
% D=  is D matrix in the State Space system, some cases are:
%     i)   constants to be estimated
%     ii)  arbitrary non-linear functions of inputs to the system (that
%          should be included as inputs to THIS function)
%     iii) Uni or multivariate transfer functions (using command vconv on inputs
%          to the system that should be included as inputs to THIS function)
%     iv)  ...
% Sigma is the covariance matrix of m noises (a(t))
m= [];
I= eye(m);
Sigma = [];
Diffy= [];
ARpoly = [];
MApoly = [];
D= vfilter();

% Translating to SS form
if stationary
    DD= I;
else
    DD= Diffy;
end
model= varmax2ss(DD, ARpoly, MApoly, Sigma, [], m, p, D);

