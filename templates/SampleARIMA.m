% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleARIMA(p, stationary)
% SampleARIMA Template for univariate ARIMA(p, d, q) models
%
% DIFFpoly(B) * y(t) = f(u(t)) + MApoly(B) / ARpoly(B) * a(t)
%
% Any of the polynomials may be defined as a product of several other
% polynomials with the help of the conv function, in this way, it is
% straightforward to build ARIMA(p, d, q)x(P, D, Q)_s models
%
% y(t):        output variable
% u(t):        set of k input variables
% a(t):        white noise
% B:           backward-shift operator (i.e. B^k * y(t)= y(t-k))
% DIFFpoly(B): [1 - B]^d
% ARpoly(B):   [1 + AR(1)*B + ... + AR(p)*B^p]
% MApoly(B):   [1 + MA(1)*B + ... + MA(q)*B^q]
%              MA(i) and MAs(i) are m x m blocks of coefficients
% f(u(t)):     relation of inputs to outputs. It may be linear, non linear
%              regression, time varying regression, multiple transfer function, etc.
%              some of the options should be implemented with the help of the
%              catsystem function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sigma is the variance of a(t), always positive
% DIFFpoly: the difference polynomial necessary to produce stationarity 
%           in the output, e.g. [1; -1] for one difference, 
%           [1; -2; 1] for two differences of all outputs, etc.
%           Use conv function to produce an arbitrary number of differences
% ARpoly:   [ARpoly(0); ARpoly(1); ...]; is a column vector of AR coefficients
% MApoly:   [MApoly(0); MApoly(1); ...]; is a column vector of MA coefficients
% D:        D matrix in the general State Space system
Sigma = [];
DIFFpoly= [];
ARpoly = [];
MApoly = [];
D= vfilter();

% Translating to SS form
if stationary
    DD= 1;
else
    DD= DIFFpoly;
end
model= varmax2ss(DD, ARpoly, MApoly, Sigma, [], 1, p, D);

