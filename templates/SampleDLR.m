% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleDLR(p, u)
% sampleDLR  Template for Dynamic Linear Regression modelling
%            (also SURE systems)
%
% y(t) = A(t) * u(t) + a(t)
%
% y(t): set of m output variables
% u(t): set of k input variables
% A(t): block of m x k time varying parameters relating inputs and outputs
% a(t): m white noises with full covariance matrix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% D is a m x Nu matrix. Ones in this matrix indicates where a parameter should be
% placed, i.e. which input (column in D) affect which output (row in D).
% E.g. if D(3, 2)= 1 one time varying parameter is being allocated between
% input 2 and output 3.
% Parameters will be the states in the system and are allocated by rows,
% i.e. first states are those affecting the first output, etc.
D= [];

% Q is the covariance matrix among the Nuxm parameters (diagonal)
% Use zeros for constant parameters
% RW (h=1) or IRW (h=2) parameters
h= 1;
Q= varmatrix(p());

% Covariance matrix of irregular component (observed noise)
H = varmatrix();

% Translating to SS form
model= dlr2ss(u, h, p, H, Q, D);

