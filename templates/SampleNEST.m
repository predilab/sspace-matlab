% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleNEST(p)
% SampleNEST  File to help writing general models by nesting inputs and 
% outputs SS systems
%          
% model= SampleNEST(p, ...)
%
% INPUTS:
%  p:   Parameter values at which the model is evaluated
%  Other inputs to this function defined by the user are allowed
%
% OUTPUTS:
%  model: Standard matrices of nested SS system
%
% See Also: SampleDHR, SampleDLR,  SampleSS, SampleVARMAX 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model_inputs= Inputs_function_call(p());
model_outputs= Outputs_function_call(p());

model= nestsystem(model_inputs, model_outputs);

D= [];
% Returning parameters
model.p= p;
