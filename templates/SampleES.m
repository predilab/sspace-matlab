% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleES(p)
% sampleES  Sample for univariate Exponential Smoothing models.
%    Hyndman, Koehler, Ord and Snyder (2008), 
%    Forecasting with exponential smoothing: the state space approach, Springer.
%
% TWO POSSIBILITIES:
% (1) Damped Trend model with seasonal and inputs:
%     y(t)= l(t-1) + Phi * b(t-1) + s(t-m) + f(u(t))  +          a(t) +  VAR(t)
%     l(t)= l(t-1) + Phi * b(t-1)                     + Alpha  * a(t)
%     b(t)=          Phi * b(t-1)                     + Beta   * a(t)
%     s(t)= s(t-m)                                    + AlphaS * a(t)
%
%     Standard constraints apply (other are possible):
%     0 <  Alpha  < 2
%     0 <  Beta   < 4-2*Alpha
%     0 <= Phi    < 1
%     0 <  AlphaS < 2
%
% (2) Damped Local level model with seasonal and inputs:
%     y(t)= Phi * l(t-1) + s(t-m) + f(u(t))  +          a(t) +  VAR(t)
%     l(t)= Phi * l(t-1) +                   + Alpha  * a(t)
%     s(t)= s(t-m)                           + AlphaS * a(t)
%
%     Standard constraints apply (other are possible):
%     Phi-1 <  Alpha  < Phi+1
%    -1 <  Phi    < 1
%     0 <  AlphaS < 2
%
% y(t):    output variable
% u(t):    set of k input variables
% l(t):    level for y(t) variable
% b(t):    slope for y(t) variable
% s(t):    seasonal for y(t) variable
% f(u(t)): relation of inputs to outputs. It may be linear, non linear
%          regression, time varying regression, multiple transfer function, etc.
%          some of the options should be implemented with the help of the
%          catsystem function
% a(t):    white noise or irregular
% Alpha:   discounting factor for level
% Beta:    discounting factor for slope
% AlphaS:  discounting factor for seasonal

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MODEL TYPE
% ModelType is the type of model required, from this eight types:
% 'ANN':  Local level model, no seasonal
% 'AAN':  Local trend, no seasonal
% 'DNN':  Damped level, no seasonal
% 'DAN':  Damped trend, no seasonal
% 'ANA#': Local level trend, additive seasonal of period #
% 'AAA#': Local trend, additive seasonal of period #
% 'DNA#': Damped level, additive seasonal of period #
% 'DAA#': Damped trend, additive seasonal of period #
ModelType= 'AAA12';

% TREND MODEL (Alphas, Betas and damping factors)
% Damping factor for damped level or damped trend models
% For multivariate models Phi is normally diagonal
% Empty for no damping factors
Phi= constrain(p(some_item), [0 1]);
% For multivariate models both Alpha and Beta are normally diagonal
% Empty Beta for local level
Alpha= constrain(p(some_item), [0 2]);
Beta=  constrain(p(some_item), [0 4-2*Alpha]);

% SEASONALITY MODEL (Alphas)
% Empty if no sesonal component is present
AlphaS= constrain(p(some_item), [0 2]);

% Add inputs if required
D= [];

% Noise variance
Sigma= varmatrix();

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= varmatrix();

% Translating to SS form
model= es2ss(ModelType, Alpha, Beta, Phi, AlphaS, D, Sigma, VAR, HVAR, p);




