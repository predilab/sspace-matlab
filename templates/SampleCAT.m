% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleCAT(p)
% SampleCAT  Template for concatenation of SS systems
%
% INPUTS:
%  p:   Parameter values at which the model is evaluated
%  Other inputs to this function defined by the user are allowed
%
% OUTPUTS:
%  model: Standard matrices of SS system

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model_1= First_function_call(p());
model_2= Second_function_call(p());
%...
model_k= Kth_function_call(p());

model= catsystem(model_1, model_2, model_k);
% Returning parameters
model.p= p;
