% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

function model= SampleSPLINE(p, t, h)
% Cubic spline interpolation with time varying variance observation noise
if nargin< 3, h= 1; end

% State noise variance
Q= varmatrix();
% Observation noise variance. MAY BE TIME VARYING according to h!!
% h should be the time varying scaled variance of dimension 1 x 1 x length(output).
H= varmatrix()*h;

% Integrated Random Walk (IRW) of Random Walk (RW) trend
% IRW= 1, for cubic spline smoothing.
IRW= 1;

% Translating to SS form
model= spline2ss(Q, H, IRW, t, p);

