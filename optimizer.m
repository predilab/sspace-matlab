% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  


DISPLAY_RESULTS= 'iter';
if isfield(sys.mat, 'dist') % Modelo no gaussiano estimaci�n m�s r�pida, pero puede ser peor
    OPTIMISER_OPTIONS = optimset('Display', DISPLAY_RESULTS, ...
        'TolFun', 1e-8, ...
        'TolX', 1e-8, ...
        'MaxFunEvals', 1000*length(sys.p0), ...
        'MaxIter', 10000, ...
        'DerivativeCheck', 'off', ...
        'LargeScale', 'off');
else
    OPTIMISER_OPTIONS = optimset('Display', DISPLAY_RESULTS, ...
        'GradObj', GradObj, ...
        'DerivativeCheck', 'off', ...
        'TolFun', 1e-8, ...
        'TolX', 1e-8, ...
        'MaxFunEvals', 1000*length(sys.p0), ...
        'MaxIter', 10000, ...
        'LargeScale', 'off');
end
%% Running optimisation
% p:           Parameter estimates
% FUN_OPTIMUM: Objective function evaluated at the optimum
% GRAD:        Gradient at the optimum
%%
if 0 % oldbfgs2    minFunc
    % Cuidado con lngt
    npar= size(sys.p0, 1);
    lngt= [max([100 npar*20]) 1e-5];
    [p, fff, i, GRAD] = oldbfgs2(sys.p0, 'llik', lngt, sys, sys.gradient, 1-sys.hidden.gr_MATLAB);
    FUN_OPTIMUM= fff(end);
elseif 0 % minFunc versi�n sofisticada
    options = [];
    options.display = 'iter';
    options.maxFunEvals = 100500;

%     options.Method = 'sd'; % Steepest Descent
%     options.Method = 'csd'; % Cyclic Steepest Descent
%     options.Method = 'bb'; options.bbType = 1; % Barzilai & Borwein
%     options.Method = 'newton0'; % Hessian-Free Newton
%     options.Method = 'pnewton0'; % Hessian-Free Newton w/ L-BFGS preconditioner
%     options.Method = 'cg'; % Conjugate Gradient
%     options.Method = 'scg'; % Scaled conjugate Gradient
%     options.Method = 'pcg'; % Preconditioned Conjugate Gradient
     options.Method = 'lbfgs'; % Default: L-BFGS (default)
    [p, FUN_OPTIMUM, ~, ~, GRAD] = minFunc(sys.OBJ_FUNCTION_NAME{1},sys.p0,options, sys, sys.gradient, 1-sys.hidden.gr_MATLAB);
elseif 0  % oldBFGS
%     ReturnValue = oldBFGS(functname, dvar0,niter,tol,lowbound,intvl,ntrials)
% the initial value of stepsize			lowbound
% the incremental value 				intvl
% the number of scanning steps	    	ntrials
    p = oldBFGS('llik', sys.p0', 100, 1e-3, 1, 1, 10, sys, 0)
elseif 0 % Mi versi�n de minfunc
    options = [];
    options.display = 'iter';
    options.maxFunEvals = 100500;

%     options.Method = 'sd'; % Steepest Descent
%     options.Method = 'csd'; % Cyclic Steepest Descent
%     options.Method = 'bb'; options.bbType = 1; % Barzilai & Borwein
%     options.Method = 'newton0'; % Hessian-Free Newton
%     options.Method = 'pnewton0'; % Hessian-Free Newton w/ L-BFGS preconditioner
%     options.Method = 'cg'; % Conjugate Gradient
%     options.Method = 'scg'; % Scaled conjugate Gradient
%      options.Method = 'pcg'; % Preconditioned Conjugate Gradient
     options.Method = 'lbfgs'; % Default: L-BFGS (default)
%     [p, FUN_OPTIMUM, ~, ~, GRAD] = fminuncSSpace(sys.OBJ_FUNCTION_NAME{1},sys.p0,options, sys, sys.gradient, 1-sys.hidden.gr_MATLAB);
    [p, FUN_OPTIMUM, exitflag, ~, GRAD, iH] = fminuncSSpace_b1(sys.OBJ_FUNCTION_NAME{1},sys.p0,options, sys, sys.gradient, 1-sys.hidden.gr_MATLAB);
    if exitflag> 0
        sys.hidden.convergence= 1;
    else
        sys.hidden.convergence= 0;
    end
    sys.hidden.covp= iH/0.5/sys.hidden.sample;
elseif 0 %%%%%%%%%% ESTE ES COMPLETAMENTE M�O
    [p, FUN_OPTIMUM, GRAD, iH] = oldBFGS1(sys, 1);
    sys.hidden.covp= iH/0.5/sys.hidden.sample;
elseif 1 % Standard de MATLAB o Octave
    try
        [p, FUN_OPTIMUM, exitflag, ooo, GRAD] = ...
                 fminunc(@(p)(sys.OBJ_FUNCTION_NAME{1}(p, sys, length(sys.hidden.exact_gr)*sys.gradient, 1-sys.hidden.gr_MATLAB)), sys.p0, OPTIMISER_OPTIONS);
%         [p, FUN_OPTIMUM, exitflag, ~, GRAD]= ...
%             fminunc(sys.OBJ_FUNCTION_NAME{1}, sys.p0, OPTIMISER_OPTIONS, sys, length(sys.hidden.exact_gr)*sys.gradient, 1-sys.hidden.gr_MATLAB);
    catch
        exitflag= 0;
        p= nan(size(sys.p0));
        FUN_OPTIMUM= nan;
        GRAD= p;
        disp('Convergence problems, line 81 optimizer')
    end
    if exitflag< 1 || (exitflag> 3 && max(abs(GRAD))> 0.01)
        sys.hidden.convergence= 0;
    else
        sys.hidden.convergence= 1;
    end
end
