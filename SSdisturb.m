function sys= SSdisturb(sys)
% SSdisturb Optimal disturbance smoother estimation with their covariance and 
%           innovations. It also runs the smoothing algorithm, then the output 
%           is the same as SSsmooth, with the addition of disturbance errors 
%           and their covariance matrices, i.e. eta_{t|N} (field .eta), 
%           cov(\eta_{t|N}) (field .Veta), eps_{t|N} (.eps) and 
%           cov(\eps_{t|N}) (field .Veps).
 %
% sys= SSdisturb(sys)
%
% INPUTS:
%  sys:    SSpace model object created with SSmodel and estimated with SSestim
%
% OUTPUTS:
%  sys:    Input system with the estimation output

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

sys= SSdisturb0(sys);

