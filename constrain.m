function P= constrain(p, limits, unconstrain)
% constrain  Transform any set of unbound values p in constrained P values 
%            between given limits. It also reverses the use, i.e. it calculates 
%            the unbound values to which bound values correspond to.
%
%   P= constrain(p, limits, unconstrain)
%
% INPUTS:
%  p:           Vector of unconstrained values (k x 1)
%  limits:      Matrix 1 x 2 or n x 2 of minimum and maximum limits
%  unconstrain: If 1 reverse the way this function works, i.e. `p` would be
%               a set of parameters within the limits
%
% OUTPUTS:
%  P:      Vector of values between given limits, with unconstrain= 1 P is
%          the unconstrain parameters corresponding to p
%
% 	Examples:
% 		constrain(0, [0 100]) is 50.
% 		constrain(50, [0 100], 1) is 0.
% 		constrain(-10, [0 100]) is 0.25.
% 		constrain(10, [0 100]) is 99.75.
% 		constrain([0; 0], [0 20; -1 10]) is [10 4.5]'.

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
if nargin< 2
    P= constrain0(p);
elseif nargin< 3
    P= constrain0(p, limits);
elseif nargin< 4
    P= constrain0(p, limits, unconstrain);
end

