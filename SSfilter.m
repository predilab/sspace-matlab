function sys= SSfilter(sys, call, Covariance)
% SSfilter  Optimal Kalman filtering estimation of states with their innovations.
%           Produces the optimal filtered estimates of states, fitted values, 
%           innovations and their covariance matrices, i.e. a_{t|t} (field .a 
%           of SSpace system, see SSmodel), P_{t|t} (.P), y}_{t|t} (.yfit),
%           cov(y_{t|t}) (.F), v_t (.v) and cov(v_t) (.Fv). 
%           Fields .yfor and .Ffor are filled in only when forecasts are required, 
%           indicated by missing values at the end of the output data .y.
%
% sys= SSfilter(sys)
%
% INPUTS:
%  sys:    SSpace model object created with SSmodel and estimated with SSestim
%
% OUTPUTS:
%  sys:    Input system with the estimation output

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

if nargin< 3, Covariance= 1; end
if nargin< 2, call= 0; end
sys= SSfilter0(sys, call, Covariance);
