% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #7: Non-Gaussian models 
%Poisson distribution: Van drivers killed in Great Britain (Durbin and Koopman, 2012, pp. 315)
%% Load and show the data
close all
clear all
load Van
y= Van(:, 1);
u= Van(:, 2);
t= (1 : 192)';

plot([y u*10+4])
%% Linear gaussian model
% Check model demo_vanl, based on SampleBSM
edit demo_vanl
%%
sys1= SSmodel('y', log(y), 'u', u, 'model', @demo_vanl, 'p0', [-1 0 -1 -1]');
sys1= SSestim(sys1);
sys1= SSvalidate(sys1);
sys1= SSsmooth(sys1);

T1= sys1.a(1, :)'+sys1.p(2)*u;
plot(t, y, 'k', t, exp(T1), 'r', t, exp(confband(T1, sys1.P(1, 1, :))), 'r:')
%% Poisson model
% Check the model demo_van_poisson. That file contains two functions, one
% based on SampleEXP, and the second one based on SampleBSM
edit demo_van_poisson
%%
% Beware that there is one parameter less than before, namely variance H,
% and that we set the seasonal variance to zero.
% We add some NaN in the middle of y(t) and some at the end to do a
% forecasting exercise
y= Van(:, 1);
u= Van(:, 2);
y(50:55)= nan;
y= [y; nan(20, 1)]; u= [u; ones(20, 1)];
tf= (1 : 192+20)';
sys= SSmodel('y', y, 'u', u, 'model', @demo_van_poisson, 'p0', sys1.p(1:2));
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSsmooth(sys);

T1= sys.a(1, :)'+sys.p(2)*u;
plot(tf, y, 'k', tf, exp(T1), 'r', tf, exp(confband(T1, sys.P(1, 1, :))), 'r:')

