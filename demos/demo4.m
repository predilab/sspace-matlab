% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #4: Multivariate Unobserved Components
% Let's apply a full multivariate DHR model to the data used in demo #4, namely the quarterly
% energy consumption data of the UK, i.e. a vector time series composed of the coal, coal+other
% solid fuels and Gas, from 1960Q1 up to 1986Q4.
%
% Estimation is now necessary, since the number of parameters now are 12
clear all
close all
load energy
y= log(energy);

plot(y)
legend('Coal', 'Coal+other', 'Gas')
%% Dynamic Harmonic Regression Model (DHR)
% Building the model is easy on the basis of SampleDHR.m
% 
% Check demo_energydhr.m (based on SampleDHR.m)
edit demo_energydhr
%%
% Estimation, forecasting and testing. Check the initial parameters.
% SSpace use analytical derivatives when estimating by MLE and when possible, 
% decreasing substantially estimation time.
% Check the time with analytical derivatives (default when you use SSmodel)
p0= repmat([-2 -2 -2 0 0 0]', 4, 1);
yfor= [y(1:104, :); nan(4, 3)];
sys= SSmodel('y', yfor, 'model', @demo_energydhr, 'p0', p0, 'user_inputs', length(yfor));
tic, sys= SSestim(sys); toc
sys= SSvalidate(sys);
%%
% It is always possible to use numerical derivatives in MLE estimation by
% turning off the 'gradient' option in SSmodel.
% Compare time estimation with the analytical derivatives!!!
sys= SSmodel('y', yfor, 'model', @demo_energydhr, 'p0', p0, ...
    'user_inputs', length(yfor), 'gradient', 0);
tic, sys= SSestim(sys); toc

sys= SSsmooth(sys);
plot([y sys.yfit'])
%%
% Estimating components and checking Trends ...
Trend= sys.a(1:3, :)';
Irregular= yfor-sys.yfit';
Seasonal= yfor-Trend-Irregular;
plot([y Trend])
%%
% ... Seasonals ... 
plot(Seasonal);
%%
% and Irregulars
plot(Irregular);
%% Basic Structural Model (BSM)
% Check demo_energybsm.m (based on SampleBSM.m)
edit demo_energybsm
%%
% Estimation, forecasting and testing. Check the initial parameters.
% SSpace use analytical derivatives when estimating by MLE and when possible, 
% decreasing substantially estimation time.
% Check the time with analytical derivatives (default when you use SSmodel)
p0= repmat([-2 -2 -2 0 0 0]', 4, 1);
yfor= [y(1:104, :); nan(4, 3)];
sys= SSmodel('y', yfor, 'model', @demo_energybsm, 'p0', p0);
tic, sys= SSestim(sys); toc
sys= SSvalidate(sys);
sys= SSsmooth(sys);
%%
% Estimating components and checking Trends ...
Trend= sys.a(1:3, :)';
Irregular= yfor-sys.yfit';
Seasonal= yfor-Trend-Irregular;
plot([y Trend])
%%
% ... Seasonals ... 
plot(Seasonal);
%%
% and Irregulars
plot(Irregular);