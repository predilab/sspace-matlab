% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #3: ARIMA and Exponential Smoothing models
% This demo implement an ARIMA and Exponential Smoothing models
% for the airpas data.
% Let's start with the ARIMA model, check the template called
% SampleARIMA
clear all
close all
edit SampleARIMA
%%
% Check now model demo_airpasarima.m (based on SampleARIMA)
% The model implemented is an ARIMA(0, 1, 1) x (0, 1, 1)_12
edit demo_airpasarima
%%
% Loading and showing data...
load airpas
y= log(airpas);
t= (1 : 144)';

plot(y)
%%
% Estimation and filtering, forecasting and testing
yfor= [y(1:132); nan(12, 1)];
sys= SSmodel('y', yfor, 'model', @demo_airpasarima);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSfilter(sys);

plot(t, y, 'k', t, sys.yfit', 'r', t, confband(sys.yfit, sys.F), 'r:')
%%
% This is one of the cases where estimation using concentrated log-likelihood is 
% advantageous. It may be done by normalising the observation variance 
% noise to 1 (H= 1) and using the concentrated log-likelihood objective function
% instead. function demo_airpasarimac implements the concentrated model.
% Results should be identical!!
edit demo_airpasarimac

sysc= SSmodel('y', yfor, 'OBJ_FUNCTION_NAME', @llikc, 'model', @demo_airpasarimac);
sysc= SSestim(sysc);
sysc= SSvalidate(sysc);
sysc= SSfilter(sysc);

plot(t, y, 'k', t, sys.yfit', 'r', t, confband(sys.yfit, sys.F), 'r:')
%%
% An Exponential Smoothing model may be also fitted to this data
% Check now model demo_airpasES.m (based on SampleES)
edit SampleES
edit demo_airpasES
%%
% Estimation, filtering, forecasting and testing
sys= SSmodel('y', yfor, 'model', @demo_airpasES);
sys= SSestim(sys);
sys= SSfilter(sys);

plot(t, y, 'k', t, sys.yfit', 'r', t, confband(sys.yfit, sys.F), 'r:')
