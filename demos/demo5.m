% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #5: Regression
% This first example is a simulation of an output variable on an AR(1)
% input and a sinusoidal wave of perio 50 observations.
clear all
close all
T= 300;
t= (1 : T)';
a= randn(T, 1)/2;
e= randn(T, 1);
u= [ones(T, 1) filter(1, [1 -0.8], a) cos(2*pi*t/50)];
y= u * [15; 4; 2] + e;

plot([y u])
legend('y', 'u');
%%
% Linear regression models may be created with the SampleSS.m function
% Check the model created for this demo, demo_lr (based on SampleSS)
% This is not an interesting option in general, but the interest is mixing
% the linear terms with other models, or nesting models. Keep reading!!
% We may implement the same model in two different ways, The standard way
% would be to use this form:
edit demo_lr_standard
%%
% For this version the way to call SSmodel is the following,
% (assuming concentrated likelihood is used):
sys= SSmodel('y', y, 'u', u', 'model', @demo_lr_standard, ...
             'OBJ_FUNCTION_NAME', @llikc);
%%
% However, a much more efficient way to implement this modle, allowed in SSpace
% the following, in which there are no system inputs, but such inputs are
% allowed to enter into the function via the additional inputs to the model
% function. Beware that matrix D is time varying, but such time variation
% is not put in the third dimension of the matrices. This is inconsistent
% with the rest of the model, but is a trick that, if aproppriately used,
% makes the code much more efficient.
% Check demo_lr
edit demo_lr
%%
% In this case, the call to SSmodel ought to be different
sys= SSmodel('y', y, 'model', @demo_lr, 'user_input', u', ...
             'OBJ_FUNCTION_NAME', @llikc);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSfilter(sys);
plot(t, y, 'k', t, sys.yfit', 'r');
%%
% Remember that you could use also another objective function, i.e. the
% minimisation of N steps ahead forecast errors. Since we are dealing with 
% a simulation, we would not expect very different estimations,
% independently of N. Try out!!
sys= SSmodel(sys, 'OBJ_FUNCTION_NAME', {@nsteps, 2});
sys= SSestim(sys);
SSvalidate(sys);
sys= SSfilter(sys);
plot(t, y, 'k', t, sys.yfit', 'r');
%%
% Now we repeat the same exercise with missing values on the output. In the
% way the model is implemented here it is also possible to estimate missing data
% in the inputs, provided there are not many. Check model demo_lrnan
edit demo_lrnan
y(100:110)= nan;
umiss= u(200:201, 2);
u(200:201, 2)= nan;
sys= SSmodel('y', y, 'model', @demo_lrnan, 'user_input', u', 'OBJ', @llikc);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSfilter(sys);
plot(t, y, 'k', t, sys.yfit', 'r');
%%
% Another way to do regression is by means of a Dynamic Linear Regression
% model with time varying parameters as Random Walks with parameter
% variances restricted to zero, i.e. the SSestim is not necessary and
% the states provides a recursive estimation of parameters.
% Check demo_dlr model based on the general template SampleDLR
edit SampleDLR
edit demo_dlr
u(200:201, 2)= umiss;
sys= SSmodel('y', y, 'model', @demo_dlr, 'user_input', u', 'OBJ', @llikc, ...
             'p', [-30; -30; -30]);
sys= SSfilter(sys);
plot(sys.a')
%%
% In this case the final estimates are the final values of the states, and
% the covariances are given by their covariance matrix at the end of the
% sample
[sys.a(:, end) sqrt(diag(sys.P(:, :, end)))]
%%
% If the smoothing algorithm is run in addition to the Kalman Filter, the
% final result is that the parameters are constant
sys= SSsmooth(sys);
plot(sys.a')
%%
% One further check is to test whether the parameters are really constant
% by avoiding any a priori assumption, i.e. we allow the parameters to vary
% over time and check whether the parameters chage over time.
% As a matter of fact, they are fairly constant!!
sys= SSmodel('y', y, 'model', @demo_dlr, 'user_input', u', 'OBJ', @llikc);
sys= SSestim(sys);
sys= SSsmooth(sys);
plot(sys.a')
%%
% The last test is done realizing that two of the inputs are deterministic
% functions of time and then perfectly forecastable, but one is stochastic
% by nature. We now try to estimate the regression model and the model for
% the input by a nested version of both models, in which a AR(1) model is
% estimated for the stochastic input and the model is nested into the
% regression model above in its standard formulation (i.e. demo_lr_standard),
% but for just one input.
% The overall model is based on the following sub-models:
% (1): demo_ar1 for the input model (based on SampleVARMAX)
% (2): demo_lr1 for the regression model with just one input
% (3): demo_lrnest for the nested model (based on SampleNEST)
edit demo_lrnest
%%
% This approach has several advantages, firstly, the regression model
% is done on the assumption of stochastic inputs; second, missing values of
% the inputs are automatically interpolated optimally; finally, forecasts
% of the input and output are directly computed. Beware that in a normal
% regression context you need to forecast the inputs in order to obtain the
% forecasts for the outputs. This is done automatically and optimally in
% this context
% Now both the output and the stochastic input are considered as outputs to
% the function
sys= SSmodel('y', [y u(:, 2)], 'model', @demo_lrnest, 'user_input', u(:, [1 3])', 'OBJ', @llikc);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSfilter(sys);

plot(sys.a')
%% Non linear regression
% Finally, let's try a non linear regression
close all
clear all
T= 300;
e= randn(T, 1)/2;
t= (1:T)';
u= [t sin(2*pi/24*t)+10];
y= 0.01*u(:, 1)+2*(u(:, 2).^(0.5))+e; plot(y)
plot(y)
%%
% Non linear regression may be estimated directly based on what have been
% already said.
% Check model demo_nlr
edit demo_nlr
%%
% Estimation, etc.
sys= SSmodel('y', y, 'model', @demo_nlr, 'user_input', u', ...
             'OBJ_FUNCTION_NAME', @llikc);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSfilter(sys);
plot(t, y, 'k', t, sys.yfit, 'r')
