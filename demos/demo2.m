% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #2: Univariate Unobserved Components models
% An Unobserved Components model decomposes a time series usually into meaningfull components, 
% though not observed. Typically the decomposition is in terms of trend, cyclical, seasonal, 
% relation to inputs and irregular components, i.e.
% 
%    z(t) = T(t) + C(t) + S(t) + I(t)
%
% This would be the observation equation, while the state equations describes the dynamic 
% behaviour of each component. 
% Many possibilities are available and may be programmed with patience based on the general 
% SampleSS.m model. But there are two pre-programmed models in SSpace:
%   (1) SampleDHR: Dynamic Harmonic Regression (DHR)
%   (2) SampleBSM: Basic Structural Model with trigonommetric seasonal (BSM)
clear all
close all
edit SampleDHR
edit SampleBSM
%%
% Let's analize the classical Airline Box-Jenkins, taken from G.P.E. Box, G.M. Jenkins, 
% G.C. Reinsel and G.M. Ljung (2015), 'Time Series Analysis Forecasting and Control', Fifth Edition,
% Wiley's Series in Probability and Statistics.
load airpas
y= log(airpas);
t= (1 : 144)';

plot(y)
%%
% A standard way to analyze this data would be to use a BSM model.
% Beware that for the seasonal component the fundamental period is 12 observations per year, 
% and the harmonics are 12/2, 12/3, 12/4, 12/5, 12/6 (6, 4, 3, 2.4, 2).
%
% Check model demo_airpasbsm.m (based on SampleBSM.m)
edit demo_airpasbsm
%%
% Estimation, prediction and filtering / smoothing may be produced by the following code,
% where the last year of data has been reserved in order to validate forecasts
% The components may also be recovered from the smoothed estimates
% Also initial conditions are specifically supplied with the input 'p0'.
% Other initial values may be tried out
yfor= [y(1:132); nan(12, 1)];
sys= SSmodel('y', yfor, 'model', @demo_airpasbsm, 'p0', -3);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSsmooth(sys);

Trend= sys.a(1, :)';
Seasonal= sum(sys.a(3:2:end, :), 1)';

plot(t, y, 'k', t, sys.yfit', 'r', t, Trend, 'r', t, Seasonal+4.5, 'k')
%% 
% All the system matrices are stored in sys.mat structure
sys.mat
%%
% In the previous example it looks like the slope variance is 0. At the
% same time we may generalize the previous model to different variances for
% each harmonic in the seasonal component. Check out function
% demo_airpasbsmc
edit demo_airpasbsmc
sys= SSmodel('y', yfor, 'model', @demo_airpasbsmc);
sys= SSestim(sys);
sys= SSsmooth(sys);

Trend= sys.a(1, :)';
Seasonal= sum(sys.a(3:2:end, :))';

plot(t, y, 'k', t, sys.yfit', 'r', t, Trend, 'r', t, Seasonal+4.5, 'k')
%%
% Let's now add an artificial tranfer function input to the time series at
% time 100
u= zeros(144, 1); u(100)= 1;
TF= filter(0.5, [1 -0.8], u);
plot([TF+5 y+TF])
%%
% And now we estimate the same model with the added input.
% Check model demo_airpasbsmu, where the u variable is added as an
% additional input to the function
% Beware that the TF component does not need to be written in SS form
edit demo_airpasbsmu
%%
% Estimation, etc.
sys1= SSmodel('y', yfor+TF, 'model', @demo_airpasbsmu, 'user', u');
sys1= SSestim(sys1);
sys1= SSvalidate(sys1);
sys1= SSsmooth(sys1);

Trend= sys1.a(1, :)';
Seasonal= sum(sys1.a(3:2:end, :))';
plot(t, y+TF, 'k', t, sys1.yfit, 'r', t, Trend, 'r', t, Seasonal+4.5, 'k')
%%
% A final example is shown when the model is not a good representation of
% the data, but still we want to have an idea of the parameter values. This
% is the case of the well-known Hodrick-Prescott filter. Check model
% demo_hp based on SampleSS in which variance matrix H plays the roll of
% lambda when matrix Q is restricted to 1 (beware that concentrated
% likelihood ought to be used)
edit demo_hp
%%
% In essence, the HP filter is like a UC model above with a smoothed trend
% and no seasonal. We know that such a model is a rather poor
% representation of these data. In such cases ML estimation is rather
% doubious. Estimation does not converge to sensible values and depend
% strongly on the initial conditions. If we start searching on 1600 the
% lambda blows up to infinity, i.e. a straight line!!
t= (1 : 168)';
yfor= [airpas; nan(24, 1)];
sys= SSmodel('y', yfor, 'model', @demo_hp, 'p0', 1600, 'OBJ_FUNCTION_NAME', @llikc);
sys= SSestim(sys);
SSvalidate(sys);
sys= SSsmooth(sys);
plot(t, yfor, 'k', t, sys.a(1, :), 'r')
%%
% If the search is started on a small value then the estimation goes to
% virtually zero, i.e. the trend is the data!!
sys= SSmodel(sys, 'p0', 0);
sys= SSestim(sys);
SSvalidate(sys);
sys= SSsmooth(sys);
plot(t, yfor, 'k', t, sys.a(1, :), 'r')
%%
% Either results are useless. One solution in this context is to use an
% objective function different to the likelihood. For example we might use
% the minimisation of the forecast errors several steps ahead. You may play
% with differente Ns and different initial conditions and check that
% results are much more sensible than before.
sys= SSmodel(sys, 'p0', 0, 'OBJ_FUNCTION_NAME', {@nsteps, [1:18*2]});
sys= SSestim(sys);
SSvalidate(sys);
sys= SSsmooth(sys);
figure(1),plot(t, yfor, 'k', t, sys.a(1, :), 'r')






