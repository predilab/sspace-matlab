% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #6: Time aggregation
% There are many situations in which data is available at different frequencies 
% along time, or interest might be important on investigating the relation 
% between two or more variables that are measured at different sampling 
% intervals. Models with time aggregation would allow to build models with 
% such properties.
%%
% Load airpassenger's data and repeat a UC model
clear all
close all
load airpas
y= log(airpas);
t= (1 : 144)';

sys= SSmodel('y', y, 'model', @demo_airpasbsm, 'p0', -4);
sys= SSestim(sys);
sys= SSsmooth(sys);

Trend= sys.a(1, :)';
Seasonal= sum(sys.a(3:2:end, :))';
Fit= Trend + Seasonal;

plot(t, y, 'k', t, Trend, 'r', t, Seasonal+4.5, 'k')
%%
% Now we repeat the analysis with the same model and data, but with the first
% five years aggregated quarterly. Check the data!
load airpas_agg
plot(t, airpas_agg, '.-')
%%
% Now check the model for the aggregated series. One important point is
% that we take advantage of the same model we already wrote for Demo 6, 
% namely demo_airpasarima.m (based on SampleAGG).
% Check it out!!
edit demo_airpasbsm_agg
%%
% There is a problem with aggregation variables when we want to use a
% variance transformation, like logarithm. This is because the logarithm of
% a sum (say 3 observations within a quarter) does not coincide with the sum
% of the log data. However, a decent approximation is taking advantage of a
% Taylor expansion following Mitchell et al. (2005) in The Economic Journal.
% By such expansion we know that for each year:
% sum(log(monthly y in quarter i)= 3*log(quarterly y in quarter i)-3*log(3)
% This means that logarithmic transformation requires a bit of elaboration.
% Check it out!!
y= [3*log(airpas_agg(1 : 60))-3*log(3); log(airpas_agg(61 : end))];
%%
% Now, we can estimate and do all the rest of the analysis. Beware that
% we need now an addiontal input that will be used to locate the time
% aggregation points
sys= SSmodel('y', y, 'model', @demo_airpasbsm_agg, 'p0', -4, 'user_input', y);
sys= SSestim(sys);
sys= SSsmooth(sys);

ATrend=    sys.a(1, :)';
ASeasonal= sum(sys.a(3:2:end, :))';
AFit= ATrend + ASeasonal;

plot(t, AFit, 'r', t, ATrend, 'k', t, ASeasonal+4.5, 'k')
%%
% Now we can compare the components estimated both ways in logs.
% Comparisons in antilogs is also possible with care, because of the non
% linear transformation!!
subplot(311), plot(t, [Trend ATrend])
subplot(312), plot(t, [Seasonal ASeasonal])
subplot(313), plot(t, [Fit AFit])
%% 
% The procedure would work as well with multivariate models in which part
% or all of the time series have the problem of time aggregation!!
