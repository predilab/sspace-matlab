% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #8: Non-linear application
% UK visitors abroad (Durbin and Koopman, 2012, pp. 312)
% Load the data
clear all
close all
load UKvisitors
y= UKvisitors/1000;
t= (1 : size(y, 1))';

plot(y)
%% Linear gaussian model
% Check linear model demo_uk based on SampleBSM with a business cycle
edit demo_uk
%%
sys1= SSmodel('y', y, 'model', @demo_uk, 'p0', -1);
sys1= SSestim(sys1);
sys1= SSvalidate(sys1);
sys1= SSsmooth(sys1);

%%
% Calculating and showing components
Trend= sys1.a(1, :)';
Cycle= sys1.a(3, :)';
Seasonal= sum(sys1.a(5:2:end, :))';

plot(t, y, 'k', t, Trend, 'r')
figure(2), plot(Cycle, 'k')
figure(3), plot(Seasonal, 'k')
%% Non-linear model with Extended Kalman Filter
% Check the model demo_uknle. 
edit demo_uknle
%%
% We add some nan in the middle of the data to make it more interesting
y(100:110)= nan;
sys= SSmodel('y', y, 'model', @demo_uknle, 'p0', [sys1.p; 0]);
sys= SSestim(sys);
sys= SSvalidate(sys);
sys= SSsmooth(sys);
%% 
% Showing Trend, Cycle, scaled and unscaled Seasonal and scaling process
Scale= exp(sys.p(7)*sys.a(1, :));
Seasonal= sum(sys.a(5:2:end, :));
figure(1), plot(t, y, 'k', t, sys.a(1, :), 'r')
figure(2), plot(sys.a(3, :))
figure(3), plot(t, Seasonal.*Scale, 'r+', t, Seasonal, 'k')
legend('Scaled seasonal', 'Unscaled seasonal')
figure(4), plot(Scale)
legend('Scale')









