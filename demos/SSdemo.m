function SSdemo(numdemo)
% SSdemo   SSpace demos
%    SSdemo(number)
%
% INPUTS:
%   number: a number from 1 to 8
%           (1) Overview and tutorial
%           (2) Univariate Unobserved Components models
%           (3) ARIMA and Exponential Smoothing models
%           (4) Multivariate Unobserved Components
%           (5) Regression
%           (6) Time aggregation
%           (7) Non-Gaussian models 
%           (8) Non-linear application

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

if numdemo== 1
    try echodemo('demo1')
    catch me
        edit demo1;
    end
elseif numdemo== 2
    try echodemo('demo2')
    catch me
        edit demo2;
    end
elseif numdemo== 3
    try echodemo('demo3')
    catch me
        edit demo3;
    end
elseif numdemo== 4
    try echodemo('demo4')
    catch me
        edit demo4;
    end
elseif numdemo== 5
    try echodemo('demo5')
    catch me
        edit demo5;
    end
elseif numdemo== 6
    try echodemo('demo6')
    catch me
        edit demo6;
    end
elseif numdemo== 7
    try echodemo('demo7')
    catch me
        edit demo7;
    end
elseif numdemo== 8
    try echodemo('demo8')
    catch me
        edit demo8;
    end
end
