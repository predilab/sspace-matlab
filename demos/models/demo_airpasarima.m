function model= demo_airpasarima(p, stationary)
Sigma = exp(2*p(3));
DIFFpoly= conv([1 -1], [1 zeros(1, 11) -1])';
ARpoly = 1;
MApoly = conv([1 p(1)], [1 zeros(1, 11) p(2)])';

D= [];

% Translating to SS form
if stationary
    DD= 1;
else
    DD= DIFFpoly;
end
model= varmax2ss(DD, ARpoly, MApoly, Sigma, [], 1, p, D);

