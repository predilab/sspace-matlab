function model= demo_lrnest(p, u)
% SampleNEST  File to help writing general models by nesting inputs and 
% outputs SS systems
%          
% model= SampleNEST(p, ...)
%
% INPUTS:
%  p:   Parameter values at which the model is evaluated
%  Other inputs to this function defined by the user are allowed
%
% OUTPUTS:
%  model: Standard matrices of nested SS system
%
% See Also: SampleDHR, SampleDLR,  SampleSS, SampleVARMAX 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model_inputs= demo_ar1(p(4:5));
model_outputs= demo_lr1(p(2));

model= nestsystem(model_inputs, model_outputs);

model.D= [p(1) p(3); 0 0]*u;
model.p= p;


function model= demo_ar1(p)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For a set o m output variables and Nu input variables
% [     I   + ARpoly(1)*B + ...] * Diffy(B) * y(t) = 
% [Omega(0) + Omega(1)*B  + ...]            * u(t) + [I + MApoly(1)*B + ...] * a(t)
% Diffy is the difference polynomial necessary to produce stationarity in the outpus, e.g.
%    [I; -I] for one difference of all outputs
%    [I; -2*I; I] for two differences of all outputs, etc.
% ARpoly = [ARpoly(0); ARpoly(1); ...]; is the matrix of AR mxm block coefficients
% MApoly = [MApoly(0); MApoly(1); ...]; is the matrix of MA mxm block coefficients
% Omega=   [ Omega(0);  Omega(1); ...]; is the matrix of Omega mxNu block coefficients
% Sigma is the covariance matrix of m noises (a(t))
m= 1;
I= eye(m);
Sigma = exp(p(2));
Diffy= 1;
ARpoly = [1 p(1)]';
MApoly = 1;
Omega= [];
% Multiple Output Multiple Input (MIMO) models are also possible by leaving
% Omega empty and specifying D below in terms of filtered inputs (that should
% be passed on to this function as an additional input). For this purpose
% use the vfilter function. D should be m x (time samples)
D= [];

% Translating to SS form
model= varmax2ss(Diffy, ARpoly, MApoly, Sigma, Omega, m, p, D);


function model= demo_lr1(p)
      
% Code defining matrix Phi (NS x Ns x (1 or N))
model.T= 0; 

% Code defining matrix Gam (Ns x (1 or N))
model.Gam= [];

% Code defining matrix E (Ns x Nw x (1 or N))
model.R= 0;

% Code defining matrix H (Nz x Ns x (1 or N))
model.Z= 1;

% Code defining matrix D (Nz x (1 or N))
model.D= p(1);

% Code defining matrix C (Nz x Nv x (1 or N))
model.C= 1;

% Code defining matrix Q (Nw x Nw x (1 or N))
model.Q= 0; 

% Code defining matrix H (Nz x Nz x (1 or N))
model.H= 1;

% Code defining matrix S (Nw x Nv x (1 or N))
model.S= 0;

