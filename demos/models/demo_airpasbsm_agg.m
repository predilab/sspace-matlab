function model= demo_airpasbsm_agg(p, y)
% sampleAGG  File to help writing models with temporal aggregation
model1= demo_airpasbsm(p);

% Translating to SS form
model= agg2ss(model1, y, p);
model.p= p;
