function model= demo_airpasarimac(p, stationary)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For a set o m output variables and Nu input variables
% [     I   + ARpoly(1)*B + ...] * Diffy(B) * y(t) = 
% [Omega(0) + Omega(1)*B  + ...]            * u(t) + [I + MApoly(1)*B + ...] * a(t)
% Diffy is the difference polynomial necessary to produce stationarity in the outpus, e.g.
%    [I; -I] for one difference of all outputs
%    [I; -2*I; I] for two differences of all outputs, etc.
% ARpoly = [ARpoly(0); ARpoly(1); ...]; is the matrix of AR mxm block coefficients
% MApoly = [MApoly(0); MApoly(1); ...]; is the matrix of MA mxm block coefficients
% Omega=   [ Omega(0);  Omega(1); ...]; is the matrix of Omega mxNu block coefficients
% Sigma is the covariance matrix of m noises (a(t))
m= 1;
I= eye(m);
Sigma = 1;
Diffy= conv([1 -1], [1 zeros(1, 11) -1])';
ARpoly = 1;
MApoly = conv([1 p(1)], [1 zeros(1, 11) p(2)])';
Omega= [];
% Multiple Output Multiple Input (MIMO) models are also possible by leaving
% Omega empty and specifying D below in terms of filtered inputs (that should
% be passed on to this function as an additional input). For this purpose
% use the vfilter function. D should be m x (time samples)
D= [];

% Translating to SS form
if stationary
    DD= I;
else
    DD= Diffy;
end
model= varmax2ss(DD, ARpoly, MApoly, Sigma, Omega, m, p, D);
