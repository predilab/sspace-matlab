function model= demo_uk(p)
% Multivariate Basic Structural Model template with dummy or 
% trigonommetric seasonality

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TREND MODEL
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
m= 1;
TT = [1 1;0 1];
ZT = [1 0];
% RT = [1 0;0 1];
% QT = blkdiag(varmatrix(p(1)), varmatrix(p(2)));
RT = [0; 1];
QT = varmatrix(p(1));

% TRIGONOMMETRIC SEASONAL MODEL
% Period are an m x Nhar matrix, where Nhar is the number of harmonics.
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
%    It is composed of block diagonal squared matrices
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs= [Q(1) Q(2) ... Q(Nhar)], where each Q(i) is a squared covariance matrix
Periods = [constrain(p(3), [18 800]) 12 6 4 3 2.4 2];
Rho = [constrain(p(4), [0.5 1]) 1 1 1 1 1 1];
Qs = [varmatrix(p(5)) repmat(varmatrix(p(6)), 1, 6)];

% DUMMY VARIABLE SEASONAL MODEL
% Period is the period of annual seasonality.
% Qs is the covariance matrix of eta disturbances for the seasonal component
% Periods = [];
% Qs = [];

% LINEAR REGRESSION TERMS
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% IRREGULAR COMPONENT
H = varmatrix(p(2));

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);


