function model= demo_airpasbsm(p)
% TREND MODEL
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= 1;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = varmatrix(p(3:4), [nan 0;0 nan]);

% TRIGONOMMETRIC SEASONAL MODEL
% Period are an m x Nhar matrix, where Nhar is the number of harmonics. NaN
%    indicate that such periodic component is not present in a time series
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs= [Q(1) Q(2) ... Q(Nhar)], where each Q(i) is a squared covariance matrix
Periods = [12 6 4 3 2.4 2];
Rho = [1 1 1 1 1 1];
Qs = repmat(varmatrix(p(1)), 1, 6);

% Input-output relations (if necessary add u(t) inputs to the inputs of this function)
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% Covariance matrix of irregular component (observed noise)
H = varmatrix(p(2));

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);

