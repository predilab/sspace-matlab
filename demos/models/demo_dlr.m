function model= demo_dlr(p, u)
% D is a m x Nu matrix. Ones in this matrix indicates where a parameter should be
% placed, i.e. which input (column in D) affect which output (row in D).
% E.g. if D(3, 2)= 1 one time varying parameter is being allocated between
% input 2 and output 3.
% Parameters will be the states in the system and are allocated by rows,
% i.e. first states are those affecting the first output, etc.
D= [1 1 1];

% Q is the covariance matrix among the Nuxm parameters (diagonal)
% Use zeros for constant parameters
% RW (h=1) or IRW (h=2) parameters
h= 1;
Q= varmatrix([p(1:3);0;0;0]);

% Covariance matrix of irregular component (observed noise)
H = 1;

% Translating to SS form
model= dlr2ss(u, h, p, H, Q, D);
