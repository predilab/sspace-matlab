function model= demo_energydhr(p, N)
% Multivariate Dynamic Harmonic Regression template
% Trend model
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= 3;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = blkdiag(varmatrix(p(1:6)), varmatrix(p(7:12)));

% Seasonal/cyclical DHR components
% Periods are an 1 x Nhar matrix, where  Nhar is the number of harmonics.
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs = [Q(1) Q(2) ... Q(Nhar)], where each Qi is a squared covariance matrix
Periods = repmat([4 2], 3, 1);
Rho = ones(3, 2);
Qs = repmat(varmatrix(p(13:18)), 1, 2);

% Linear Regression effects (if necessary add u(t) inputs to the inputs of this function)
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% Covariance matrix of irregular component (observed noise)
H = varmatrix(p(19:24));

% Translating to SS form
model= vdhr2ss(TT, ZT, RT, QT, Periods, Rho, Qs, N, H, D, VAR, HVAR, m, p);

