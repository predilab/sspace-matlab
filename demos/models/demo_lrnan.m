function model= demo_lrnan(p, u)
      
% Code defining matrix Phi (NS x Ns x (1 or N))
model.T= 0; 

% Code defining matrix Gam (Ns x (1 or N))
model.Gam= [];

% Code defining matrix E (Ns x Nw x (1 or N))
model.R= 0;

% Code defining matrix H (Nz x Ns x (1 or N))
model.Z= 0;

% Code defining matrix D (Nz x (1 or N))
u(2, 200)= p(4); u(2, 201)= p(5);
model.D= [p(1) p(2) p(3)]*u;

% Code defining matrix C (Nz x Nv x (1 or N))
model.C= 1;

% Code defining matrix Q (Nw x Nw x (1 or N))
model.Q= 0; 

% Code defining matrix H (Nz x Nz x (1 or N))
model.H= 1;

% Code defining matrix S (Nw x Nv x (1 or N))
model.S= 0;

% Returning parameters
model.p= p;

