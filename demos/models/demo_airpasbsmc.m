function model= demo_airpasbsmc(p)
% Trend model
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= 1;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = [varmatrix(p(1)) 0; 0 0];

Periods = [12 6 4 3 2.4 2];
Rho = [1 1 1 1 1 1];
Qs = [varmatrix(p(3)) varmatrix(p(4)) varmatrix(p(5)) ...
      varmatrix(p(6)) varmatrix(p(7)) varmatrix(p(8))];

% Input-output relations (if necessary add u(t) inputs to the inputs of this function)
D= [];

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% Covariance matrix of irregular component (observed noise)
H = varmatrix(p(2));

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);

