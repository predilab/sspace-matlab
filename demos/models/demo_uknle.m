function model= demo_uknle(p, at, ctrl)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model1= demo_uk(p(1:6));

%% Defining linear matrices
if ctrl< 2
    % Matrix definitions as in SampleSS
    % Code defining matrix T (ns x ns x (1 or n))
    model.T= model1.T; 
    % Code defining matrix Gam (empty, ns x (1 or n), ns x Nu, ns x Nu x (1 or n))
    model.Gam= [];
    % Code defining matrix R (ns x neps x (1 or n))
    model.R= model1.R;
    % Code defining matrix Z (Ny x ns x (1 or n))
    model.Z= [];
    % Code defining matrix D (empty, Ny x (1 or n), Ny x Nu, Ny x Nu x (1 or n))
    model.D= [];
    % Code defining matrix C (Ny x Neps x (1 or n))
    model.C= 1;
    % Code defining matrix Q (neps x neps x (1 or n))
    model.Q= model1.Q; 
    % Code defining matrix H (Ny x Ny x (1 or n))
    model.H= model1.H;
    % Returning parameters
    model.p= p;
    model.S= [];
end

%% Defining nonlinear matrices in State Equation
if any(ctrl== [2 0])
    % Code defining derivative of matrix T(at) (ns x Nsigma)
    % NECESSARY ONLY IN CASE EXTENDED KALMAN FILTERED IS USED!!
    model.dTa= [];
    % Code defining matrix T(at) (ns x Nsigma)
    model.Ta= [];
    % Code defining matrix R(at) (ns x neps x (1 or n))
    model.Ra= [];
    % Code defining matrix Q(at) (neps x neps x (1 or n))
    model.Qa= []; 
end

%% Defining nonlinear matrices in Observation Equation
if any(ctrl== [3 0])
    b= p(7);
    expfun= exp(b*at(1));
    S= sum(at(5:2:15));
    % Code defining derivative of matrix Z(at) (m x Nsigma)
    % NECESSARY ONLY IN CASE EXTENDED KALMAN FILTERED IS USED!!
    model.dZa= [1+b*expfun*S 0 1 0 expfun 0 expfun 0 expfun 0 expfun 0 expfun 0 expfun];
    % Code defining matrix Z(at) (m x Nsigma)
    model.Za= at(1)+at(3)+expfun*S;
    % Code defining matrix C(at) (Ny x Neps x (1 or n))
    model.Ca= [];
    % Code defining matrix H(at) (Ny x Ny x (1 or n))
    model.Ha= [];
end
model.p= p;
end
