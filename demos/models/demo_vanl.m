function model= demo_vanl(p)
% TREND MODEL
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
% m is the number of outputs
m= 1;
TT = 1;
ZT = 1;
RT = 1;
QT = exp(p(1));

% TRIGONOMMETRIC SEASONAL MODEL
% Period are an m x Nhar matrix, where Nhar is the number of harmonics.
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
%    It is composed of block diagonal squared matrices
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs= [Q(1) Q(2) ... Q(Nhar)], where each Q(i) is a squared covariance matrix
Periods = [];
Rho = [];
Qs = [];

% DUMMY VARIABLE SEASONAL MODEL
% Period is the period of annual seasonality.
% Qs is the covariance matrix of eta disturbances for the seasonal component
Periods = 12;
Qs = exp(p(3));

% LINEAR REGRESSION TERMS
D= p(2);

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% IRREGULAR COMPONENT
H = exp(p(4));

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);


