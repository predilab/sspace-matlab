function model= demo_airpasbsmu(p, u)
m= 1;
I= eye(m); O= zeros(m);
TT = [I I;O I];
ZT = [I O];
RT = [I O; O I];
QT = varmatrix(p(3:4), [nan 0;0 nan]);

Periods = [12 6 4 3 2.4 2];
Rho = [1 1 1 1 1 1];
Qs = repmat(varmatrix(p(1)), 1, 6);

% Input-output relations (if necessary add u(t) inputs to the inputs of this function)
D= filter(p(5), [1 p(6)], u);

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% Covariance matrix of irregular component (observed noise)
H = varmatrix(p(2));

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);
