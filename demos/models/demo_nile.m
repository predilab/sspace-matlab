function model= demo_nile(p)
% Model for the Nile data

% Code defining matrix Phi
model.T= 1; 

% Code defining matrix Gam
model.Gam= [];

% Code defining matrix E
model.R= 1;

% Code defining matrix H
model.Z= 1;

% Code defining matrix D
model.D= [];

% Code defining matrix C
model.C= 1;

% Code defining matrix Q
model.Q= 10.^p(1); 

% Code defining matrix H
model.H= 10.^p(2);

% Code defining matrix S
model.S= [];

% Returning parameters
model.p= p;

