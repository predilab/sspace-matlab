function model= demo_airpasES(p)
% MODEL TYPE
% ModelType is the type of model required, from this six types:
% 'ANN':  Local level model, no seasonal
% 'AAN':  Local trend, no seasonal
% 'DNN':  Damped level, no seasonal
% 'DAN':  Damped trend, no seasonal
% 'ANA#': Local level trend, additive seasonal of period #
% 'AAA#': Local trend, additive seasonal of period #
% 'DNA#': Damped level, additive seasonal of period #
% 'DAA#': Damped trend, additive seasonal of period #
ModelType= 'AAA12';

% TREND MODEL (Alphas, Betas and damping factors)
% Damping factor for damped level or damped trend models
% For multivariate models Phi is normally diagonal
% Empty for no damping factors
Phi= [];
% For multivariate models both Alpha and Beta are normally diagonal
% Empty Beta for local level
Alpha= constrain(p(1), [0 2]);
Beta=  constrain(p(2), [0 4-2*Alpha]);

% SEASONALITY MODEL (Alphas)
% Empty if no sesonal component is present
AlphaS= constrain(p(3), [0 2]);

% Add inputs if required
D= [];

% Noise variance
Sigma= varmatrix(p(4));

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% Translating to SS form
model= es2ss(ModelType, Alpha, Beta, Phi, AlphaS, D, Sigma, VAR, HVAR, p);



