function model= demo_hpinn(p)

% Code defining matrix T (ns x ns x (1 or n))
model.T= [1 1;0 1]; 

% Code defining matrix Gam (empty, ns x (1 or n), ns x Nu, ns x Nu x (1 or n))
model.Gam= [];

% Code defining matrix R (ns x neps x (1 or n))
model.R= p(1:2); %[0; 1];

% Code defining matrix Z (Ny x ns x (1 or n))
model.Z= [1 0];

% Code defining matrix D (empty, Ny x (1 or n), Ny x Nu, Ny x Nu x (1 or n))
model.D= [];

% Code defining matrix C (Ny x Neps x (1 or n))
model.C= 1;

% Code defining matrix Q (neps x neps x (1 or n))
model.Q= p(3); %1; 

% Code defining matrix H (Ny x Ny x (1 or n))
model.H= p(3);

% Code defining matrix S (empty, neta x Neps x (1 or n))
model.S= p(3);

% Returning parameters
model.p= p;


