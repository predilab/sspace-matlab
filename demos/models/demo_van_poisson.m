function model= demo_van_poisson(p, H)
% Models with observation equation from the exponential family of models
% p (yt|?t) = exp[yt'?t ? bt(?t) + ct(yt)]
% Beware that the 'H' input to this function, i.e. the observationa noise
%   variance, is compulsory and should be passed on to the SSsystem in line 11.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General SS system of your choice
model= demo_van(p, H);

% Choose any member of the exponential family from these 
%       for the observation equation:
%   Binary
%   Binomial(n)
%   Exponential
%   NegativeBinomial
%   Poisson
model.dist= Poisson;
% Returning parameters
model.p= p;
model.param= {};

function model= demo_van(p, H)
% Multivariate Basic Structural Model template with dummy or 
% trigonommetric seasonality

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLEASE, DO NOT REMOVE ANYTHING:
%     JUST ADD WHAT IS NECESSARY TO SET UP THE MODEL 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TREND MODEL
%    aT(t+1) = TT * aT(t) + RT * etaT(t)
%      yT(t) = ZT * aT(t)
%     QT= COV(etaT(t));
m= 1;
TT = 1;
ZT = 1;
RT = 1;
QT = exp(p(1));

% TRIGONOMMETRIC SEASONAL MODEL
% Period are an m x Nhar matrix, where Nhar is the number of harmonics.
% Rho are the damping factors affecting each periodic component (same dimensions as Periods)
%    It is composed of block diagonal squared matrices
% Qs is the covariance matrix of eta disturbances for the harmonics
%    Qs= [Q(1) Q(2) ... Q(Nhar)], where each Q(i) is a squared covariance matrix
Periods = [];
Rho = [];
Qs = [];

% DUMMY VARIABLE SEASONAL MODEL
% Period is the period of annual seasonality.
% Qs is the covariance matrix of eta disturbances for the seasonal component
Periods = 12;
Qs = 0;

% LINEAR REGRESSION TERMS
D= p(2);

% VAR polynomial including leading ones and variance of noise
VAR= [];
HVAR= [];

% IRREGULAR COMPONENT
% H= [];

% Translating to SS form
model= bsm2ss(TT, ZT, RT, QT, Periods, Rho, Qs, H, D, VAR, HVAR, m, p);



