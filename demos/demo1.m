% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

%% SSpace Demo #1: Overview and tutorial
% This demo is an overview of the possibilities of SSpace toolbox.
% Any feedback is welcome:
%   Diego.Pedregal@uclm.es or Marco.Villegas@uclm.es
%%
% SSpace is a toolbox for State Space modelling in a very flexible way, in
% a crafty way.
% State Space modelling in SSpace toolbox comprises 4 specific steps:
%    (1) Write the model in a paper and make sure that it is correct and is exactly 
%        what you want
%    (2) Code model (1) into a MATLAB function
%    (3) Estimate unknown parameters in your model. Show results and
%        perform initial validation checks on your model
%    (4) Run the Kalman Filter and optionally the State Fixed Interval Smoothing 
%        and/or Disturbance Smoothing algorithms in order to get optimal estimates
%        of states, disturbances and residuals
%
% Usually these steps would be preceded by exploratory analysis and identification of any
% kind (for that purpose you should use the tools you prefer) and would be
% followed by validation/diagnostic checks on residuals after the analysis is completed.
%
% The next slides will show how these steps are done with the SSpace toolbox.
%% 
% FIRST STEP: Write the model in a paper and make sure that it is correct and is exactly 
%             what the user wants to do
%
% This is probably the most important step and the one than most users tend to skip, 
% converting the experience with SSpace in a painfull process with a lot of time wasted.
% Only users with a long experience on SS modelling (or when the SS form of the model 
% to use is very well known) may skip this very first step without wasting a lot
% of time.
% Believe us: writing the model in a paper saves a lot of time.
% 
% Assume we want a model of the following type (for a scalar output):
%                             1      
%    y(t) = level + noise = ----- eta(t) + eps(t)
%                           (1-B)
% (B is the backshift operator such that B^k*z(t)= z(t-k))
% The SS representation is:
%    a(t+1) = a(t) + eta(t)       Var(eta(t))= Q    Cov(eta(t), eps(t))= 0
%    y(t)   = a(t) + eps(t)       Var(eps(t))= 1
%% 
% FIRST STEP: Write the model in a paper and make sure that it is correct and is exactly 
%             what the user wants to do
%
% Here is the previous SS representation once more:
%    a(t+1) = a(t) + eta(t)          Var(eta(t))= Q      Cov(eta(t), eps(t))= 0
%    y(t)   = a(t) + eps(t)          Var(eps(t))= 1
%
% The general SS model implemented in SSpace have various forms, for the moment we may say:
%    a(t+1) = T(t) a(t) + Gam(t) + R(t) eta(t)
%    y(t)   = Z(t) a(t) + D(t)   + C(t) eps(t)
%
%    Var(eta(t))= Q(t)     Var(eps(t))= H(t)    Cov(eta(t), eps(t))= S(t)
%
% Comparing both systems we have that:
%    T(t)= R(t)= Z(t)= C(t)= 1, Q(t)= Q, H(t)= 1, S(t)= 0, and Gam(t) and D(t) do not exist
%
% We have now a clear idea of the system
%%
% SECOND STEP: Create a function that translates the model in (1) into computer code
%
% Models may always be written from scratch, by filling in the SampleSS.m file
%
% The way to proceed is 
%    (a) Edit sampleSS.m
%    (b) Save it with a different name preferably in a different folder
%    (c) Fill in the function keeping in mind two simple rules: (1) do not remove anything
%        but add any code necessary to define the model; (2) not compulsory, but healthy:
%        reserve 'p' as the variable name for the unknown parameters to estimate later on.
clear all
close all
edit SampleSS
%%
% SECOND STEP: Create a function that translates the model in (1) into your own code
%
% There are other functions similar to SampleSS.m that helps in writting models appropriately. 
% Always follow the same rules as before.
% These functions are:
%    SampleSS       - General SS system
%    SampleBSM      - Basic Structural Model
%    SampleDHR      - Dynamic Harmonic Regression
%    SampleDLR      - Dynamic Linear Regression
%    SampleES       - Exponential smoothing
%    SampleARIMA    - ARIMA models with exogenous inputs
%    SampleEXP      - Non gaussian models (exponential family of models)
%    SampleNEST     - Nesting State Space systems
%    SampleNONGAUSS - Non gaussian models
%    SampleSV       - Non gaussian models (Stochastich Volatility)
%    SampleNL       - Non Linear model (estiated with Extended Kalman Filter)
%    SampleAGG      - Models with time aggregation problems
%    SampleCAT      - Concatenation of State Space systems
%
% The system in the previous slides is already written in demo_nile.m
%    (T(t)= R(t)= A(t)= 1, Q(t)= Q, H(t)= 1, S(t)= 0, and Gam(t) and D(t) do not exist)
% Beware that, since Q is a variance parameter, it should be positive.
% Here you have one possible parameterisation. Check it out!
edit demo_nile
%%
% SECOND STEP: Create a function that translates the model in (1) into your own code
%
% Let's apply the previous model to the annual flow volume of the Nile river at Aswan
% from 1871 to 1970. If a model wanted to be estimated, the logical first steps would
% be to load the data and analysing them in a exploratory way.
% Remember that the first Aswan dam was finsihed in 1902, i.e. observation 32
load nile
y= nile;
t= (1881 : 1880+size(y, 1))';

plot(t, nile)
%%
% THIRD STEP: Estimate unknown parameters in model
%
% You need to create an SSpace model object. Use SSmodel function for that purpose.
% In this example the model object has output data y, the model is written in the function
% demo_nile above and we want to start the search for the unknown parameters from p0= [1; 1]
sys= SSmodel('y', y, 'model', @demo_nile);
%%
% THIRD STEP: Estimate unknown parameters in model
%
% Once the model is created we estimate the unknown parameters by Maximum Likelihood
% with the SSestim function. This function takes the object created, estimates the model
% and fills the object with the output. You can check the parameter estimates in sys.p, 
% or, alternatively run SSvalidate on your model.
% The typical call is very simple:
sys= SSestim(sys);
sys= SSvalidate(sys);
%%
% FOURTH STEP: Run the Kalman Filter, state smoother and/or disturbance smoother
%              in order to get optimal estimates of states, disturbances and residuals
%
% The syntax once more is very simple, for the filtered output:
sys= SSfilter(sys);
%%
% Appropriateness of the model may be checked by graphical tools ... 
figure(1)
clf, plot(t, sys.y, 'k', t, sys.a, 'r', t, confband(sys.a, sys.P), 'r:')
%%
% ... and also analyzing the innovations, since they should be independent Gaussian
% white noise. They may be normalize with the help of the normalize function.
plot(t, sys.v)
%%
% Further tests usually mentioned are the standardised disturbances and the
% smoothed output. They may be normalized with the help of the normalize function
sys= SSdisturb(sys);
plot(t, sys.y, 'k', t, sys.yfit, 'r', t, confband(sys.yfit, sys.F), 'r:')
figure(2), plot(t, sys.eta, 'k', t, sys.eps, 'r')
legend('Transition error', 'Observation error');
%%
% Obviously, the model in this demo is simple, but SSpace
% supports much more complex models, like Multiple-Input-Multiple-Output
% systems, Time Varying Systems, etc. Those features allow for an incredibly wide
% range of options. Some of them not yet explored...
%
% In the next demos many examples will be shown, some of them taken
% from books or papers. The purpose of the examples is just to show how
% this particular toolbox works, and by no means the authors pretend to claim
% that the particular analyses presented here are the best possible!!


