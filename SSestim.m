function sys= SSestim(sys)
% SSestim   Estimation of unkwnown parameters in SS systems. 
%           It fills in the fields .p, .obj_value, .llik and 
%           .mat of a :keyword:`SSpace` system (see SSmodel)
%
% sys= SSestim(sys)
%
% INPUT:
%  sys:    SSpace model object created with SSmodel
%
% OUTPUT:
%  sys:    Input system with the estimation output

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

sys= SSestim0(sys);
