function [y, difpoly]= vdiff(x, order, s)
% VDIFF    Differenciation of a vector of variables
%
%    zd= vdiff(z, order, s)
%
% INPUTS:
%  z:    Vector time series data (m x T or T x m)
%  order: Difference orders for multiplicative polynomials (1 x any, default is 1)
%  s:     Seasonal parameter for each polynomial (1 x any, default is 1)
%
% OUTPUTS:
%  zd:    Differenced data (m x <T)
%
%  Examples:
%    z= randn(500, 3);
%    vdif(z, [2 1], [1 12])
%    vdif(z, [2 1; 1 0], [1 12])
%    vdif(z, [2 1; 1 0; 2 1], [1 12; 0 6])
%    vdif(z, [1 1 1], [1 24 168])
if nargin< 2
    [y, difpoly]= vdiff0(x);
elseif nargin< 3
    [y, difpoly]= vdiff0(x, order);
elseif nargin< 4
    [y, difpoly]= vdiff0(x, order, s);
end
    
