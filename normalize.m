function Z= normalize(z, Pz)
% normalize  Normalize a vector of variables y with time varying covariance matrix P
%
%   Y= normalize(y, P);
%
% INPUTS:
%  y: Output data (m x n  or  n x m)
%  P: Matrix of covariance matrices of innovations (m x m x n), typically the output of
%     SSfilter or any other core function
%
% OUTPUTS:
%  Y:   Normalized variable, variables y transformed with identity covariance matrix

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

Z= normalize0(z, Pz);
