function sys= SSsmooth(sys)
% SSsmooth  Optimal Kalman filtering estimation of states with their innovations.
%           Produces the optimal smoothed estimates of states, fitted values, 
%           innovations and their covariance matrices, i.e. a_{t|N} (field .a 
%           of SSpace system, see SSmodel), P_{t|N} (.P), y}_{t|N} (.yfit),
%           cov(y_{t|N}) (.F), v_t (.v) and cov(v_t) (.Fv). 
%           Fields .yfor and .Ffor are filled in only when forecasts are required, 
%           indicated by missing values at the end of the output data .y.
%
% sys= SSsmooth(sys)
%
% INPUTS:
%  sys:    SSpace model object created with SSmodel and estimated with SSestim
%
% OUTPUTS:
%  sys:    Input system with the estimation output

% Copyright (C) 2016 Diego J. Pedregal & Marco A. Villegas. All rights reserved.
%
% This file is part of SSpace.
% 
% SSpace is free software: you can redistribute it and/or modify
% 
% SSpace is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

sys= SSsmooth0(sys);

